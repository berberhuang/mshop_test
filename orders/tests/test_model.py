from datetime import datetime, timedelta 
from django.test import TestCase
from ..models import Order, Product, OrderItem


class OrderModelTestClass(TestCase):

    @classmethod
    def setupTestData(cls):
        for num in range(20):
            Product.objects.create(name='Product {}'.format(num))

        for _ in range(10):
            o = Order.objects.create(customer_id=1, shipping=0)
            o.created_at=(datetime.now()).date()
            o.save()

        for _ in range(20):
            o = Order.objects.create(customer_id=2, shipping=80)
            o.created_at=(datetime.now()).date()
            o.save()

        for _ in range(30):
            o = Order.objects.create(customer_id=2, shipping=80)
            o.created_at=(datetime.now()+timedelta(days=1)).date()
            o.save()

        for n in range(0, 20):
            OrderItem.objects.create(order_id=n, product_id=1, qty=3)

        for n in range(20, 30):
            OrderItem.objects.create(order_id=n, product_id=2, qty=1)

        for n in range(30, 40):
            OrderItem.objects.create(order_id=n, product_id=3, qty=2)

        for n in range(40, 50):
            OrderItem.objects.create(order_id=n, product_id=4, qty=1)

    def setUp(self):
        OrderModelTestClass.setupTestData()

    def test_get_best_sellers_product(self):
        p = Product.objects.get_best_sellers(None, None)
        self.assertEqual(p[0]['product'], 1)
        self.assertEqual(p[1]['product'], 3)

    def test_get_shipping_free_count(self):
        r = Order.objects.get_shipping_free_count(None, None)
        self.assertEqual(r['free_count'], 10)
        self.assertEqual(r['not_free_count'], 50)

    def test_get_order_retention(self):
        r = Order.objects.get_order_retention()
        self.assertEqual(len(r), 2)
        self.assertEqual(r[0]['user_count'], 2)
        self.assertEqual(r[0]['day_number'], 0)

        self.assertEqual(r[1]['user_count'], 1)
        self.assertEqual(r[1]['day_number'], 1)

        