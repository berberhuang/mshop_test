from django.db import models, connection
from django.db.models import Sum, Min, Count, Case, When, Value, BooleanField, Subquery


class OrderItemQuerySet(models.QuerySet):
    def get_top_product(self, top_count, start_date, end_date):
        return [[self.filter(product=1).get().product]]


class OrderQuerySet(models.QuerySet):
    order_item_manager = OrderItemQuerySet.as_manager()

    def get_shipping_free_count(self, start_date, end_date):
        result = (
            self.values('shipping')
            .annotate(
                is_free=Case(
                    When(shipping=0, then=Value(True)),
                    default=Value(False),
                    output_field=BooleanField(),
                )
            )
            .values('is_free')
            .annotate(count=Count(1))
        )

        free = 0
        not_free = 0
        for r in result:
            if r['is_free']:
                free = r['count']
            else:
                not_free = r['count']

        return {'free_count': free, 'not_free_count': not_free}

    def get_order_retention(self):
        with connection.cursor() as cursor:
            cursor.execute(
                'WITH cohort_items as '
                '(SELECT customer_id, MIN(created_at) AS first_buy FROM `order` GROUP BY customer_id),'
                'customer_activates as '
                '(SELECT A.customer_id, CAST((JulianDay(created_at) - JulianDay(B.first_buy)) AS INTEGER) AS number_of_day FROM `order` A LEFT JOIN cohort_items B ON A.customer_id = B.customer_id GROUP BY A.customer_id, strftime(\'%m-%d\', created_at)),'
                'retention_table as '
                '(SELECT strftime(\'%m-%d\', A.first_buy) AS cohort_day, B.number_of_day, count(1) AS user_count FROM cohort_items A LEFT JOIN customer_activates B ON A.customer_id = B.customer_id GROUP BY cohort_day, B.number_of_day)'
                'SELECT cohort_day, number_of_day, user_count FROM retention_table order by cohort_day, number_of_day;'
            )
            return [{
                'cohort_day': row[0],
                'day_number': row[1],
                'user_count': row[2]
            } for row in cursor.fetchall()]
        return []

class ProductManager(models.Manager):
    def get_best_sellers(self, start_date, end_date):
        result = (
            OrderItem.objects
            .values('product')
            .annotate(s_qty=Sum('qty'))
            .order_by('-s_qty')
            .values('s_qty', 'product', 'product__name')[:10]
        )

        return [
            {
                'product': p['product'],
                'name': p['product__name'],
                'qty': p['s_qty'],
            } for p in result
        ]


class Product(models.Model):
    name = models.CharField(max_length=30)
    last_modify_date = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    objects = ProductManager()

    @classmethod
    def get_sentinel_instance(cls):
        return Product.get_or_create(name="deleted")[0]

    class Meta:
        db_table = "product"

    def __str__(self):
        return "Product:{}, id:{}".format(self.name, self.id)


class Order(models.Model):
    customer_id = models.IntegerField()
    shipping = models.DecimalField(max_digits=4, decimal_places=0)
    last_modify_date = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    objects = OrderQuerySet.as_manager()

    class Meta:
        db_table = 'order'

    def __str__(self):
        return "Order:{}, customer_id:{}, created_at:{}".format(self.id, self.customer_id, self.created_at)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(
        Product, on_delete=models.SET(Product.get_sentinel_instance))
    qty = models.IntegerField()

    class Meta:
        db_table = 'order_item'

    def __str__(self):
        return "OrderItem:{}, order_id:{}, product_id:{}, qty:{}".format(self.id, self.order, self.product, self.qty)
