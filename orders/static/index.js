
$(function(){
    var top3sellers = $('#top3sellers');

    google.charts.load('current', {'packages':['corechart', 'table']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        $.get('/order/shippingfree', function(resp){
            var data = google.visualization.arrayToDataTable([
                ['Type', 'Count'],
                ['Free',  resp['free_count']],
                ['Not Free', resp['not_free_count']]
            ]);

            var chart = new google.visualization.PieChart(document.getElementById('shipping_free_chart'));
            chart.draw(data);
        });

        $.get('/order/retention', function(resp){
            // convert data to Google chart api supported format
            var max_day_num = 0;
            var cohort_item_map={}
            for(var i in resp){
                var cohort_day = resp[i]['cohort_day'];
                var day_number = resp[i]['day_number'];
                if(day_number > max_day_num){
                    max_day_num = day_number;
                }

                if(!cohort_item_map[cohort_day]){
                    cohort_item_map[cohort_day]={};
                }
                cohort_item_map[cohort_day][day_number]=resp[i];

            }
            
            var rows_data=[];
            var cur_cohort_day=''
            for(var i in resp){
                if(cur_cohort_day === resp[i]['cohort_day']){
                    continue;
                }
                cur_cohort_day = resp[i]['cohort_day'];
                var row =[];
                row.push(cur_cohort_day)
                for(var i=0; i<=max_day_num; i+=1){
                    var d = cohort_item_map[cur_cohort_day][i];
                    if(d){
                        row.push(d['user_count']);
                    }else{
                        row.push(0);
                    }
                }
                rows_data.push(row);
            }

            // draw the table
            var data = new google.visualization.DataTable();
            data.addColumn('string', '初次訂購月');
            for(var i=0; i<=max_day_num; i+=1) {
                data.addColumn('number', 'day '+i);
            }
            data.addRows(rows_data);
            var table = new google.visualization.Table(document.getElementById('cohort_table'));
            table.draw(data);
        });
    }

    $.get("/order/product/bestseller", function(result){
        var cur_qty = -1;
        var order = 0;
        for(var i=0; order<3 && i<result.length; ++i){
            product=result[i]
            if(cur_qty != product.qty) {
                order+=1;
                cur_qty = product.qty
            }
            top3sellers.append('<li><span>'+ product['name'] + '</span> qty:<span>'+product['qty']+'</span></li>');
        }
    });

    
});
