from django.urls import path
from . import views

urlpatterns = [
    path('product/bestseller', views.best_seller_product),
    path('shippingfree', views.shipping_free_count),
    path('retention', views.order_retention),
]
