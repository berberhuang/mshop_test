import json
from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.http import require_http_methods
from .models import Product, Order

@require_http_methods(['GET'])
def best_seller_product(request):
    r = Product.objects.get_best_sellers(None, None)
    return HttpResponse(json.dumps(r), content_type='application/json')

@require_http_methods(['GET'])
def shipping_free_count(request):
    c = Order.objects.get_shipping_free_count(None, None)
    return HttpResponse(json.dumps(c), content_type='application/json')

@require_http_methods(['GET'])
def order_retention(request):
    c = Order.objects.get_order_retention()
    return HttpResponse(json.dumps(c), content_type='application/json')

@require_http_methods(['GET'])
def index(request):
    return render(request, 'index.html')
